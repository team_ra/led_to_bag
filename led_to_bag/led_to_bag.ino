/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 1
 */
#include "utilities.h"

/*
 * Startup temp var to know if the led movement
 * is going up or down 
 * true = down
 * false = up
 */
bool down;

/*
 * Fading brightness level (0..510)
 */
int brightness;

/*
 * Level (1...8)
 */
int level;

void setup() {
  // Reset all variables to startup values
  score = 0;
  gameStatus = GAMESTATUS_INIT;
  lastDebounceTime = 0;
  brightness = 0;
  down = true;
  // Switch on first led
  ledOn(1);
  // Set pin mode
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);
  pinMode(LED5, OUTPUT);
  pinMode(POT, INPUT);
  pinMode(TS, INPUT);
  pinMode(TD, INPUT);
  Serial.begin(9600);
  // Interrupts to check if buttons are pressed
  attachInterrupt(digitalPinToInterrupt(TS), startGame, RISING);
  attachInterrupt(digitalPinToInterrupt(TD), nextLed, RISING);
  randomSeed(analogRead(A5));
  Serial.println("Welcome to Led to Bag. Press Key TS to Start");
}

void loop() {
  switch (gameStatus) {
    // Ts just pressed
    case GAMESTATUS_STARTGAME:
      // Update level timer
      level = (analogRead(POT) * (MAX_LEVEL - 1) / MAX_POT_VALUE) + 1;
      timer = MIN_TIME + ((MAX_LEVEL - level) * DELTA_TIME);
      // Game startup: switch off all leds and switch game status to "STARTLEVEL"
      ledOn(0);
      Serial.println("Go!");
      gameStatus = GAMESTATUS_STARTLEVEL;
      delay(1000);
      // Update time left manager variable
      timerEnd = millis() + timer;
      break;

    // Generation of random starting position (1..3)
    case GAMESTATUS_STARTLEVEL:
      ledOn(random(1, 4));
      gameStatus = GAMESTATUS_EXEC;
      break;

    // Game execution: state check
    case GAMESTATUS_EXEC:
      if (millis() > timerEnd) { // Time is over
        if (currentLedOn == 4) {
          // Object in the bag: next level
          nextLevel();
        } else {
          // Object outside: game over
          gameStatus = GAMESTATUS_LOSE;
        }
      } else if (currentLedOn == 4) { // While there's still time to play and the object is in the bag
        // Fade the bag led
        brightness += FADE_AMOUNT;
        if (brightness > MAX_LED_BRIGHTNESS * 2) {
          brightness = 0;
        }
        ledOn(LED4, brightness <= MAX_LED_BRIGHTNESS ? brightness : MAX_LED_BRIGHTNESS * 2 - brightness);
        delay(10);
      }
      break;

    // Game over
    case GAMESTATUS_LOSE:
      loseGame();
      break;

    // GAMESTATUS_INIT - game not started: leds up-down
    default:
      delay(500);
      if (currentLedOn == 2) {
        ledOn(down ? 3 : 1);
      } else {
        down = (currentLedOn == 1);
        ledOn(2);
      }
  }
}
