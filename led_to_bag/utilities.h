/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 1
 */
#ifndef __UTILITIES__
  #define __UTILITIES__

  #define LED1 7
  #define LED2 6
  #define LED3 5
  #define LED4 9
  #define LED5 4
  #define POT A1
  #define TS 3
  #define TD 2
  #define GAMESTATUS_INIT 0
  #define GAMESTATUS_STARTGAME 1
  #define GAMESTATUS_STARTLEVEL 2
  #define GAMESTATUS_EXEC 3
  #define GAMESTATUS_LOSE 4
  #define MAX_POT_VALUE 1023
  #define MAX_LEVEL 8
  #define MIN_TIME 6000
  #define DELTA_TIME 500
  #define MAX_LED_BRIGHTNESS 255
  #define FADE_AMOUNT 5
  #define TIMER_DIVIDER 8
  #define DEBOUNCE_DELAY 180

  extern int score;
  extern volatile int gameStatus;
  extern volatile unsigned long lastDebounceTime;
  extern unsigned int timer;
  extern unsigned long timerEnd;
  extern volatile int currentLedOn;

  void ledOn(int led);
  void ledOn(int led, int intensity);
  void loseGame();
  void startGame();
  void nextLed();
  void nextLevel();
#endif
