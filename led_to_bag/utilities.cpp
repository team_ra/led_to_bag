/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 1
 */
#include "utilities.h"
#include "Arduino.h"

/*
 * Number of points scored
 */
int score;

/*
 * Game status during program execution (1..4)
 */
volatile int gameStatus;

/*
 * Temp util variable to prevent button press bounce
 */
volatile unsigned long lastDebounceTime;

/*
 * Time left management variables
 */
unsigned int timer;
unsigned long timerEnd;

/*
 * Current on led (1..5)
 */
volatile int currentLedOn;

// Switch on led <led> (digital on, intensity: 255) and switch off the others
// NOTE: works only for the not bag leds (digital leds)
void ledOn(int led) {
  currentLedOn = led;
  digitalWrite(LED1, led == 1 ? HIGH : LOW);
  digitalWrite(LED2, led == 2 ? HIGH : LOW);
  digitalWrite(LED3, led == 3 ? HIGH : LOW);
  digitalWrite(LED5, led == 5 ? HIGH : LOW);
  analogWrite(LED4, 0);
}

// Switch on led <led> (analog on, intensity: <intensity>) and switch off the others
// NOTE: works only for the white bag led (analog led)
void ledOn(int led, int intensity) {
  digitalWrite(LED1, LOW);
  digitalWrite(LED2, LOW);
  digitalWrite(LED3, LOW);
  digitalWrite(LED5, LOW);
  analogWrite(LED4, intensity);
}

// Game over
void loseGame() {
  // Switch on red led
  ledOn(5);
  // Send to serial interface the game over info
  Serial.print("Game Over - Score: ");
  Serial.println(score);
  // Reset score and game status
  score = 0;
  gameStatus = GAMESTATUS_INIT;
  delay(2000);
}

// Interrupt function called on Ts button press
void startGame() {
  // Switch game status to STARTGAME
  if (gameStatus == GAMESTATUS_INIT) {
    gameStatus = GAMESTATUS_STARTGAME;
  }
}

// Interrupt function called on Td button press
void nextLed() {
  // Prevent button press bounce
  if ((millis() - lastDebounceTime) > DEBOUNCE_DELAY) {
    lastDebounceTime = millis();
    // Switch on next led only if the game is in execution and the on led is not the red one
    if (gameStatus == GAMESTATUS_EXEC && currentLedOn <= 4) {
      currentLedOn++;
      if (currentLedOn == 5) { // Red led is on: switch game status to LOSE
        gameStatus = GAMESTATUS_LOSE;
      } else if (currentLedOn == 4) { // Bag led is on: send status to serial interface
        Serial.print("Another object in the bag! Count: ");
        Serial.print(score + 1);
        Serial.println(" objects");
      } else { // All the other leds: switch on the corresponding led
        ledOn(currentLedOn);
      }
    }
  }
}

// Go to next level
void nextLevel() {
  // Update score, gamestatus and time left management variables
  score++;
  gameStatus = GAMESTATUS_STARTLEVEL;
  timer = timer - (timer / TIMER_DIVIDER);
  timerEnd = millis() + timer;
}
